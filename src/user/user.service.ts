import { Inject, Injectable, UseGuards } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UserService {

  constructor(
    @Inject('USER_REPOSITORY')
    private userRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async login (email:string,password:string) {
   const user = await this.userRepository.findOne({email:email});
   if(!user){
     throw new Error("Not Found");
     
   }
   if (user && user.password === password) {
    const { password, ...result } = user;
    return {
      access_token: this.jwtService.sign(result),
    };
  } else {
    throw new Error("Password does not match");
    
  }
   
  }
  create(createUserDto: CreateUserDto) {
    return this.userRepository.create(createUserDto);
  }

  async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async findOne(id: number) : Promise<User> {
    return this.userRepository.findOne(id);
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    return this.userRepository.update(id,updateUserDto);
  }

  async remove(id: number) {
    return this.userRepository.delete(id);
  }
}
