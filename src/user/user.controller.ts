import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { JwtAuthGuard } from 'src/auth/jwt.auth.guard';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/login')
  async login(@Body() loginInput: any) {
    const{ email, password}= loginInput;
    try {
      return await this.userService.login(email, password);
    } catch (error) {
      return {error:error}
    }
 
  }

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    try {
      return await this.userService.create(createUserDto);
    } catch (error) {
      return {error:error}
    }
   
  }
  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll() {
    try {
      return this.userService.findAll();
    } catch (error) {
      return {error:''}
    }
   
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try{
      return this.userService.findOne(+id);
    } catch (error) {
      return {error:error}
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    try{
      return this.userService.update(+id, updateUserDto);
    } catch (error) {
      return {error:error}
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try{
      return this.userService.remove(+id);
    } catch (error) {
      return {error:error}
    }
  }
}
