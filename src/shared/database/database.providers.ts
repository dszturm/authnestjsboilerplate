import { createConnection } from 'typeorm';
import { User } from '../../user/entities/user.entity';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () => await createConnection({
      type: 'postgres',
      host: '127.0.01',
      port: 5432,
      username: 'postgres',
      password: 'postgres',
      database: 'teste',
      entities: [
         User
      ],
      synchronize: true,
    }),
  },
];